from django.apps import AppConfig


class IplDataProjectConfig(AppConfig):
    name = 'ipl_data_project'
