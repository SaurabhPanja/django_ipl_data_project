from ipl_data_project.models import Matches, Deliveries
from django.core.management.base import BaseCommand

class Command(BaseCommand):
     def handle(self, *args, **kwargs):
        match_count = Matches.objects.from_csv('./matches.csv')
        delivery_count = Deliveries.objects.from_csv('./deliveries.csv')
        print("{} matches inserted\n {} deliveries inserted".format(match_count, delivery_count))
