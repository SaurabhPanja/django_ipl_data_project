from django.db import models
from postgres_copy import CopyManager

# Create your models here.

class Match(models.Model):
    # id = models.IntegerField(primary_key=True)
    season = models.CharField(max_length=200, null=True)
    city = models.CharField(max_length=200, null=True)
    date = models.CharField(max_length=200, null=True)
    team1 = models.CharField(max_length=200, null=True)
    team2 = models.CharField(max_length=200, null=True)
    toss_winner = models.CharField(max_length=200, null=True)
    toss_decision = models.CharField(max_length=200, null=True)
    result = models.CharField(max_length=200, null=True)
    dl_applied = models.CharField(max_length=200, null=True)
    winner = models.CharField(max_length=200, null=True)
    win_by_runs = models.CharField(max_length=200, null=True)
    win_by_wickets = models.CharField(max_length=200, null=True)
    player_of_match = models.CharField(max_length=200, null=True)
    venue = models.CharField(max_length=200, null=True)
    umpire1 = models.CharField(max_length=200, null=True)
    umpire2 = models.CharField(max_length=200, null=True)
    umpire3 = models.CharField(max_length=200, null=True)
    objects = CopyManager()


class Delivery(models.Model):
    match_id = models.ForeignKey(Match, on_delete=models.CASCADE)
    inning = models.FloatField(null=True)
    batting_team = models.CharField(max_length=200, null=True)
    bowling_team = models.CharField(max_length=200, null=True)
    over = models.CharField(max_length=200, null=True)
    ball = models.CharField(max_length=200, null=True)
    batsman = models.CharField(max_length=200, null=True)
    non_striker = models.CharField(max_length=200, null=True)
    bowler = models.CharField(max_length=200, null=True)
    is_super_over = models.CharField(max_length=200, null=True)
    wide_runs = models.FloatField(null=True)
    bye_runs = models.FloatField(null=True)
    legbye_runs = models.FloatField(null=True)
    noball_runs = models.FloatField(null=True)
    penalty_runs = models.FloatField(null=True)
    batsman_runs = models.FloatField(null=True)
    extra_runs = models.FloatField(null=True)
    total_runs = models.FloatField(null=True)
    player_dismissed = models.CharField(max_length=200, null=True)
    dismissal_kind = models.CharField(max_length=200, null=True)
    fielder = models.CharField(max_length=200, null=True)
    objects = CopyManager()




