window.onload = () =>{
    let problem1Button = document.querySelector('#problem-1'),
        problem2Button = document.querySelector('#problem-2'),
        problem3Button = document.querySelector('#problem-3'),
        problem4Button = document.querySelector('#problem-4'),
        problem5Button = document.querySelector('#problem-5');
    
    problem1Button.addEventListener('click', getChart(url='/api/matches_played_per_year/', xTitleText="Matches Played Per Season", yTitleText='Number of Matches', ySeriesName='Matches Played', xSeriesName='Season'));

    problem2Button.addEventListener('click', getStackedChart(url='/api/matches_won_by_teams_over_years/'));

    problem3Button.addEventListener('click', getChart(url='/api/year_2016_extra_runs_team/' , xTitleText="Extra Runs Conceded in 2016", yTitleText='Extra Runs', ySeriesName='Runs Conceded',xSeriesName='Team Name'));

    problem4Button.addEventListener('click', getChart(url='/api/top_economical_bowler_2015/' , xTitleText="Top economical bowler in 2015", yTitleText='economies', ySeriesName='economy', xSeriesName='Player Name'));

    problem5Button.addEventListener('click', getChart(url='/api/leading_run_scorer/' , xTitleText="All time leading run scorer", yTitleText='Runs scored in 1000(s)', ySeriesName='Runs', xSeriesName='Player Name'));
        
}

const getChart = (url, xTitleText, yTitleText, ySeriesName, xSeriesName) =>{
    return ()=>fetch(url)
    .then(res=>res.json())
    .then(result=>getXY(result))
    .then(XYData=>drawChart(xTitleText=xTitleText, yTitleText=yTitleText, ySeriesName=ySeriesName, xSeriesName=xSeriesName, xCategoriesData = XYData[0], yCategoriesData = XYData[1]))
}

const getXY = (apiResult) =>{
    let x = [],
        y = [];
    for(let key in apiResult){
        x.push(key)
        y.push(apiResult[key])
    }
    return [x,y]
}

const drawChart = (xTitleText, yTitleText, ySeriesName, xSeriesName, xCategoriesData, yCategoriesData) =>{
    let randomColor = '#'+(function lol(m,s,c){return s[m.floor(m.random() * s.length)] + (c && lol(m,s,c-1));})(Math,'0123456789ABCDEF',4);

    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            // text: 'Monthly Average Rainfall'
            text : xTitleText
        },
        xAxis: {
            categories: xCategoriesData,
            crosshair: true,
            title : {
                text : xSeriesName
            }
        },
        yAxis: {
            min: 0,
            title: {
                // text: 'Matches'
                text : yTitleText
            }
        },
        series: [{
            // name: 'Tokyo',
            name : ySeriesName,
            data: yCategoriesData,
            color : randomColor
    
        }]
    });}

const getStackedChart = (url) =>{
    return ()=>fetch(url)
    .then(res=>res.json())
    .then(result=>getData(result))
    .then(chartData=>drawStackedChart(xData=chartData[0], yData=chartData[1]))
}

const getData = (apiResult) =>{
    let stackedData = [],
        yData = [],
        xData = [];
    for(let key in apiResult){
        stackedData.push(key)
        yStackedData = []
        for(let innerKey in apiResult[key]){
            yStackedData.push(apiResult[key][innerKey])
        }
        yData.push(yStackedData)
    }
    for(let key in apiResult){
        for(let innerKey in apiResult[key]){
            xData.push(innerKey)
        }
        break;
    }

    let chartYSeries = []
    for(let i = 0; i < yData.length; i++){
        let randomColor = '#'+(function lol(m,s,c){return s[m.floor(m.random() * s.length)] + (c && lol(m,s,c-1));})(Math,'0123456789ABCDEF',4);
        chartYSeries.push({'name':stackedData[i] , 'data' : yData[i], 'color':randomColor})
    }
    // console.log(xData)
    // console.log(chartYSeries)
    return [xData, chartYSeries]
}

const drawStackedChart = (xData, yData) => {
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Matches won by teams in every season'
        },
        xAxis: {
            categories : xData,
            // title : {
            //     text : 'Season'
            // }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Matches'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -10,
            verticalAlign: 'top',
            y: -18,
            floating: true,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        series : yData
    });
}
