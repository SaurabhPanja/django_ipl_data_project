def matches_won_teams_year(request):
    matches_won_by_all_teams = {}
    match_queryset = Matches.objects.values('winner','season').annotate(Count('season'))
    # years = []
    # for year in range(2008,2018):
        # years.append(str(year))
    
    for match in match_queryset:
        team = match['winner']
        season = match['season']
        wins = match['season__count']
        if team in matches_won_by_all_teams:
            if season in matches_won_by_all_teams[team]:
                matches_won_by_all_teams[team][season] += wins
            else:
                matches_won_by_all_teams[team][season] = wins
        else:
            matches_won_by_all_teams[team] = {season : wins}
    print(matches_won_by_all_teams)
    return JsonResponse(matches_won_by_all_teams