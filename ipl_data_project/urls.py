from django.urls import path

from . import views
from .views import *

urlpatterns = [
    path('', views.index, name='index'),
    path('api/matches_played_per_year/', views.matches_played_per_year),
    path('api/matches_won_by_teams_over_years/', views.matches_won_teams_year),
    path('api/year_2016_extra_runs_team/', views.year_2016_extra_runs_team),
    path('api/top_economical_bowler_2015/', views.top_economical_bowler_2015),
    path('api/leading_run_scorer/', views.leading_run_scorer),
    path('api/deliveries/<int:id>', views.get_delivery_api),
    path('api/matches/<int:id>', views.get_match_api),
    path('api/matches/', views.show_or_create_match),
    path('api/deliveries/', views.show_or_create_delivery),
    path('matches/<int:pk>', MatchDetailView.as_view()),
    path('deliveries/<int:pk>', DeliveryDetailView.as_view()),
    path('matches/', CreateMatchView.as_view()),
    path('deliveries/', CreateDeliveryView.as_view()),
]