from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, JsonResponse
from django.template import loader
from .models import *
from django.db.models import *
from pprint import pprint
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
import json
from .serializers import MatchSerializer, DeliverySerializer
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import status

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
# Create your views here.


@cache_page(CACHE_TTL)
def index(request):
    return render(request, 'ipl_data_project/index.html')

@cache_page(CACHE_TTL)
def matches_played_per_year(request):
    match_queryset = Match.objects.values('season').annotate(Count('season'))
    matches_played_per_year = {}
    for match in match_queryset:
        matches_played_per_year[match['season']] = match['season__count']
    return JsonResponse(matches_played_per_year)

@cache_page(CACHE_TTL)
def matches_won_teams_year(request):
    matches_won_by_all_teams = {}
    match_queryset = Match.objects.values('winner','season').annotate(Count('season'))

    # for year in range(2008,2018):
        # print(year)
    
    for match in match_queryset:
        team = match['winner']
        season = match['season']
        wins = match['season__count']
        if team in matches_won_by_all_teams:
            if season in matches_won_by_all_teams[team]:
                matches_won_by_all_teams[team][season] += wins
            else:
                matches_won_by_all_teams[team][season] = wins
        else:
            matches_won_by_all_teams[team] = {season : wins}
    
    for team, season_wins in matches_won_by_all_teams.items():
        all_seasons = []
        for season in season_wins.keys():
            all_seasons.append(season)
        for year in range(2008,2018):
            year = str(year)
            if year not in all_seasons:
                matches_won_by_all_teams[team][year] = 0
    return JsonResponse(matches_won_by_all_teams)

@cache_page(CACHE_TTL)
def year_2016_extra_runs_team(request):
    year_2016_extra_runs_team = {}
    year_2016_extra_runs_team_query_set = Delivery.objects.filter(match_id__season='2016', is_super_over = 0).values('bowling_team').order_by('bowling_team').annotate(total_extra_runs=Sum('extra_runs'))
    for extra_runs_team in year_2016_extra_runs_team_query_set:
        year_2016_extra_runs_team[extra_runs_team['bowling_team']] = extra_runs_team['total_extra_runs']
    return JsonResponse(year_2016_extra_runs_team)

@cache_page(CACHE_TTL)
def top_economical_bowler_2015(request):
    top_economical_bowler_2015 = {}
    economical_bowlers_query_set =  Delivery.objects.filter(match_id__season='2015').values('bowler').annotate(economy=(Sum('batsman_runs')+Sum('wide_runs')+Sum('noball_runs'))*6.0/Count('bowler', filter=Q(wide_runs=0, noball_runs=0), output_field=FloatField())).order_by('economy')[:10]
    for bowler in economical_bowlers_query_set:
        top_economical_bowler_2015[bowler['bowler']] = round(bowler['economy'], 2)
    return JsonResponse(top_economical_bowler_2015)

@cache_page(CACHE_TTL)
def leading_run_scorer(request):
    leading_run_scorer = {}
    leading_run_scorer_query_set = Delivery.objects.values('batsman').annotate(total_runs = Sum('batsman_runs', filter=Q(is_super_over = '0'))).order_by('-total_runs')[:10]
    for batsman in leading_run_scorer_query_set:
        leading_run_scorer[batsman['batsman']] = batsman['total_runs']
    return JsonResponse(leading_run_scorer)

def show_or_create_match(request):
    if request.method == 'GET':
        all_matches = list(Match.objects.all().values()[0:10])
        if request.GET:
            limit = int(request.GET['limit'])
            offset = int(request.GET['offset'])
            all_matches = list(Match.objects.all().values()[offset:limit+offset])
        return JsonResponse({'match':all_matches})
    
    elif request.method == 'POST':
        match = Match()
        match_data = json.loads(request.body)
        id = Match.objects.all().count() + 1
        match_data['id'] = id
        for key in match_data:
            if hasattr(match, key):
                setattr(match, key, match_data[key])
            else:
                return "Thik se type kar sale!!"
        match.save()
        return HttpResponse(status=201)

def show_or_create_delivery(request):
    if request.method == 'POST':
        delivery = Delivery()
        delivery_data = json.loads(request.body)
        id = Delivery.objects.all().count() + 1
        delivery_data['id'] = id
        for key in delivery_data:
            if hasattr(delivery, key):
                setattr(delivery, key, delivery_data[key])
            else:
                return "Thik se type kar sale!!"
        delivery.save()
        return HttpResponse(status=201)

    elif request.method == 'GET':
        all_deliveries = list(Delivery.objects.all().values()[0:120])
        if request.GET:
            limit = int(request.GET['limit']) or 0
            offset = int(request.GET['offset']) or 120
            all_deliveries = list(Delivery.objects.all().values()[offset:limit])
        return JsonResponse({'delivery' : all_deliveries})



def get_delivery_api(request,id):
    if request.method == 'DELETE':
        delivery = Delivery.objects.get(pk=id).delete()
        delivery = {'result':"deleted"}
    elif request.method == 'GET':
        delivery = Delivery.objects.filter(pk=id).values()
        if delivery.count() == 0:
            return JsonResponse({'error' : 'No such delivery with  id {}'.format(id)})
        return JsonResponse(delivery[0])
    elif request.method == 'PUT':
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        Delivery.objects.filter(pk=id).update(**body)
        delivery = {'result': 'updated'}
    return JsonResponse(delivery, safe=False)

def get_match_api(request,id):
    if request.method == 'DELETE':
        match = Match.objects.get(pk=id).delete()
        match = {'result':"deleted"}
    elif request.method == 'GET':
        match = Match.objects.filter(pk=id).values()
        if match.count() == 0:
            return JsonResponse({'error' : 'No such match with  id {}'.format(id)})
        return JsonResponse(match[0])
    elif request.method == 'PUT':
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        Match.objects.filter(pk=id).update(**body)
        match = {'result': 'updated'}

    return JsonResponse(match, safe=False)

class MatchDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Match.objects.all()
    serializer_class = MatchSerializer

    def get(self, request, *args, **kwargs):
        try:
            a_match = self.queryset.get(pk=kwargs["pk"])
            return Response(MatchSerializer(a_match).data)
        except Match.DoesNotExist:
            return Response(
                data={
                    "message": "Match with id: {} does not exist".format(kwargs["pk"])
                },
                status=status.HTTP_404_NOT_FOUND
            )

class DeliveryDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Delivery.objects.all()
    serializer_class = DeliverySerializer

    def get(self, request, *args, **kwargs):
        try:
            a_delivery = self.queryset.get(pk=kwargs["pk"])
            return Response(DeliverySerializer(a_delivery).data)
        except Delivery.DoesNotExist:
            return Response(
                data={
                    "message": "Delivery with id: {} does not exist".format(kwargs["pk"])
                },
                status=status.HTTP_404_NOT_FOUND
            )

class CreateMatchView(generics.ListCreateAPIView):
    serializer_class = MatchSerializer
    queryset = Match.objects.all()

class CreateDeliveryView(generics.ListCreateAPIView):
    serializer_class = DeliverySerializer
    queryset = Delivery.objects.all()