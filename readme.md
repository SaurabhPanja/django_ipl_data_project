# Django IPL Data Project

* The application is built using **Django** and **Django Rest Framework** 
* It draws chart by manipulating given ipl data using highcharts
* It provides data over http following rest conventions using **Django Rest Famework**

### Dependencies

* Django
* Django REST Framework
* Highcharts
* PostgreSQL

### Installation

``` pip install -r requirements.txt```

### Run

``` cd mysite && python manage.py runserver ```